import React from 'react';
import ReactDOM from 'react-dom';
import './lib/css/index.scss';
import './lib/css/r-notifs.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));

